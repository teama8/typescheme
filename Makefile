TRG = typescheme

UNAME = $(shell uname)

ifneq ($(UNAME), Linux)
	ifneq ($(UNAME), Darwin)
		FixPath = $(subst /,\,$1)
		RM = del /Q
	endif
else
	FixPath = $1
	RM = rm -f
endif

LIBD = $(call FixPath,./lib/)
BIND = $(call FixPath,./bin/)
SRCD = $(call FixPath,./src/)

UNFIXED_SRCD = ./src/

make:
	mkdir -p $(LIBD)
	mkdir -p $(BIND)
	bison -vdy -o$(LIBD)$(TRG).tab.c $(SRCD)$(TRG).y
	flex -l -o$(LIBD)$(TRG).yy.c $(SRCD)$(TRG).l
	gcc -I$(UNFIXED_SRCD) $(SRCD)utils.c $(SRCD)operations.c $(SRCD)SymbolTable.c $(SRCD)TypedValue.c $(SRCD)StringReplace.c $(LIBD)$(TRG).tab.c $(LIBD)$(TRG).yy.c -o$(BIND)$(TRG)

clean:
	$(RM) $(LIBD)* $(BIND)*

