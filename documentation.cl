; Comments start with semicolons.

; Typescheme is written as lists of
; things inside parentheses, separated
; by whitespace.

; Math
(+ 1 2) ; => 3
(- 2 1) ; => 1
(* 2 3) ; => 6
(/ 6 2) ; => 3

; Expressions can be nested
(+ (- 3 2) (* 2 3)) ; => 7
; We can use line breaks everywhere
(+
    (* 1 2)
    (- 4 1)) ; => 5

; Increment and decrement operations
(inc 1) ; => 2
(dec (dec 3)) ; => 1

; Support for floating point values
.5 ; => 0.500000
(+ 5.5 .5) ; => 6.000000

; If one of the operands is a float the result
; is a float value
(/ 10 3.0) ; => 3.333333 

; Booleans
#t ; => true
#f ; => false

; Comparisons
(> 3 2) ; => #t
(< 3 2) ; => #f
(= 5 (+ 2 3)) ; => #t
(<= 0 (* 0 1)) ; => #t

; Logic operations
(not #t) ; => #f
(or #f #t) ; => #t
(and #t #f) ; => #f

; Conditional expressions (ternary operation)
(if #t 3 4) ; => 3
(if #f 3 4) ; => 4
(if (= 5 (+ 2 3)) 5 6) ; => 5

; Strings
"Hello, world!" ; => "Hello, world!"

; Backslash is an escaping character
; TODO: works only with `\n` for now
"Hello,\nworld!" ; => "Hello,
;world!"

; Variables ca be set with `var`
(var a 3)
a ; => 3

; Reassign variables
(set a 4)
a ; => 4

; Variables can be declared using
; the result of an expression
(var c 
    (if #t 2 3))
c ; => 2

; Variables can be shadowed creating
; a local scope, variables declared within
; a `local` block have only local scope
(local
    (var c "Hello")
    (display c)) ; prints "Hello"
c ; => 2

; The concat function
; Values and variables can be concatenated to strings
(concat "Hello" ", world!") ; => "Hello, world!"
(var d 5)
(concat "d: " d) ; => "d: 5"

; Output to STDOUT
(display "Hello, world!") ; prints "Hello, world!"

; Read from STDIN
; `read` prompts the user for a value and stores it
; in a variable
(read n "Enter a value for n: ")

; Works for numeric and literal values
(read name "Enter your name: ")
(display (concat 
    "Hello, " 
    (concat name "!")))

; TODO:
; Lists
;(cons 1 '()) ; => (1)
;(cons 1 (cons 2 (cons 3 '()))) ; => (1 2 3)

; TODO:
; Local scoping
; Variables declared with let have only local scope
;(let ((me "Bob"))
;    "Alice"
;    me) ; TODO: ???

; TODO:
; Functions
;(fn sum (x y)
;    (+ x y)) ; defines function sum
;(sum 2 3) ; => 5