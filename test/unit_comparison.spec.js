"use strict";

/**
 * Comparison operations test
 */

// Imports
var chai = require('chai'),
    path = require('path'),
    shell = require('shelljs');
const assert = chai.assert;
const expect = chai.expect;

const bin = path.join(__dirname, '../bin/typescheme');

const run = (inst) => {
    return shell
        .ShellString(inst)
        .exec(`"${bin}"`, { silent: true });
};

describe('Comparison operations', function() {

    describe('Errors', function() {

        it('(< 3 "s") => TypeError (STRING)', function() {
            let inst = '(< 3 "s")';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string("> TypeError", "> STRING");
        });

        it('(!= #t 3) => TypeError (BOOLEAN)', function() {
            let inst = '(!= #t 3)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string("> TypeError", "> BOOLEAN");
        });
    });

    describe('<', function() {

        it('(< 5 6) => #t', function() {
            let inst = '(< 5 6)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });

        it('(< 3.0 2) => #f', function() {
            let inst = '(< 3.0 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });
    });

    describe('<=', function() {

        it('(<= 3 3) => #t', function() {
            let inst = '(<= 3 3)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });

        it('(<= 4 3.5) => #f', function() {
            let inst = '(<= 4 3.5)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });
    });

    describe('>', function() {

        it('(> 3 5) => #f', function() {
            let inst = '(> 3 5)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });

        it('(> 2.5 2) => #t', function() {
            let inst = '(> 2.5 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });
    });

    describe('>=', function() {

        it('(>= 4 5) => #f', function() {
            let inst = '(>= 4 5)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });

        it('(>= 2.0 2) => #t', function() {
            let inst = '(>= 2.0 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });
    });

    describe('=', function() {

        it('(= 4 3) => #f', function() {
            let inst = '(= 4 3)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });

        it('(= 3.0 3) => #t', function() {
            let inst = '(= 3.0 3)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });
    });

    describe('!=', function() {

        it('(!= 0 0.0) => #f', function() {
            let inst = '(!= 0 0.0)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });

        it('(!= 3.1 3) => #t', function() {
            let inst = '(!= 3.1 3)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });
    });
});
