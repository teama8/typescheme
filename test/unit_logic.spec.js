"use strict";

/**
 * Logic operations test
 */

// Imports
var chai = require('chai'),
    path = require('path'),
    shell = require('shelljs');
const assert = chai.assert;
const expect = chai.expect;

const bin = path.join(__dirname, '../bin/typescheme');

const run = (inst) => {
    return shell
        .ShellString(inst)
        .exec(`"${bin}"`, { silent: true });
};

describe('Logic operations', function() {

    describe('Errors', function() {

        it('(not 1) => TypeError (INTEGER)', function() {
            let inst = '(not 1)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string("> TypeError", "> STRING");
        });

        it('(+ 2 #t) => TypeError (STRING)', function() {
            let inst = '(and "2" #t)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string("> TypeError", "> STRING");
        });

        it('(or #f 3.5) => TypeError (FLOAT)', function() {
            let inst = '(or #f 3.5)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string("> TypeError", "> FLOAT");
        });
    });

    describe('And', function() {

        it('(and #t #t) => #t', function() {
            let inst = '(and #t #t)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });

        it('(and #f #t) => #f', function() {
            let inst = '(and #f #t)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });
    });

    describe('Or', function() {

        it('(or #t #f) => #t', function() {
            let inst = '(or #t #f)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });

        it('(or #f #f) => #f', function() {
            let inst = '(or #f #f)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });
    });

    describe('Not', function() {

        it('(not #t) => #f', function() {
            let inst = '(not #t)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #f");
        });

        it('(not #f) => #t', function() {
            let inst = '(not #f)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> #t");
        });
    });
});
