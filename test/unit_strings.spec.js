"use strict";

/**
 * String operations test
 */

// Imports
var chai = require('chai'),
    path = require('path'),
    shell = require('shelljs');
const assert = chai.assert;
const expect = chai.expect;

const bin = path.join(__dirname, '../bin/typescheme');

const run = (inst) => {
    return shell
        .ShellString(inst)
        .exec(`"${bin}"`, { silent: true });
};

describe('String operations', function() {

    describe('Concatenation', function() {

        it('(concat "ab" "cd") => "abcd"', function() {
            let inst = '(concat "ab" "cd")';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> "abcd"');
        });

        it('(concat 1 2) => "12"', function() {
            let inst = '(concat 1 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> "12"');
        });

        it('(concat "a " #t) => "a #t"', function() {
            let inst = '(concat "a " #t)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> "a #t"');
        });
        
        it('(concat (concat "a" "b") "c") => "abc"', function() {
            let inst = '(concat (concat "a" "b") "c")';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> "abc"');
        });
    });
});
