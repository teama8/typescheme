"use strict";

/**
 * Builtins test
 */

// Imports
var chai = require('chai'),
    path = require('path'),
    shell = require('shelljs');
const assert = chai.assert;
const expect = chai.expect;

const bin = path.join(__dirname, '../bin/typescheme');

const run = (inst) => {
    return shell
        .ShellString(inst)
        .exec(`"${bin}"`, { silent: true });
};

describe('Builtins', function() {

    describe('display', function() {

        it('should print to STDOUT', function() {
            let inst = '(display "Hello")';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> Hello');
        });
    });

    describe('var', function() {

        it('should store a variable', function() {
            let inst = '(var c 3)(display c)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> 3');
        });

        it('should display an error if a variable is already declared', function() {
            let inst = '(var c 3)(var c "Hello")(display c)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string('> ReferenceError', 'already declared');
        });

        it('should display an error if a variable is not declared', function() {
            let inst = '(display c)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string('> ReferenceError', "not declared");
        });
    });

    describe('set', function() {

        it('should allow to overwrite a variable', function() {
            let inst = '(var c 3)(set c 4)c';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> 4');
        });

        it('should display an error if a variable is not declared', function() {
            let inst = '(set c 4)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string('> ReferenceError', "not declared");
        });
    });

    describe('if', function() {

        it('should return first value if condition true', function() {
            let inst = '(if #t 1 "2")';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> 1');
        });

        it('should return second value if condition false', function() {
            let inst = '(if #f 1 "2")';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string('> "2"');
        });
    });

    describe('local', function() {

        it('should open a local scoped block', function() {
            let inst = `
            (var c 3)
            (local
                (var c "Hello")
                (display c))
            c
            `
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string('> Hello', '> 3');
        });
    });

    describe('comments', function() {

        it('should ignore comments', function() {
            let inst = ';COMMENT';
            let stdout = run(inst).stdout;
            expect(stdout).to.not.contain.string('COMMENT');
        });
    });
});
