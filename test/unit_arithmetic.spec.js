"use strict";

/**
 * Artihmetic operations test
 */

// Imports
var chai = require('chai'),
    path = require('path'),
    shell = require('shelljs');
const assert = chai.assert;
const expect = chai.expect;

const bin = path.join(__dirname, '../bin/typescheme');

const run = (inst) => {
    return shell
        .ShellString(inst)
        .exec(`"${bin}"`, { silent: true });
};

describe('Arithmetic operations', function() {

    describe('Errors', function() {

        it('(/ 9 0) => DivideByZeroError', function() {
            let inst = '(/ 9 0)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> DivideByZeroError");
        });

        it('(+ 9) => ArityError', function() {
            let inst = '(+ 9)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> ArityError");
        });

        it('(inc 9 5) => ArityError', function() {
            let inst = '(inc 9 5)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> ArityError");
        });

        it('(inc "2") => TypeError (STRING)', function() {
            let inst = '(inc "2")';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string("> TypeError", "> STRING");
        });

        it('(+ 2 #t) => TypeError (BOOLEAN)', function() {
            let inst = '(+ 2 #t)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.all
                .string("> TypeError", "> BOOLEAN");
        });
    });

    describe('Addition', function() {

        it('(+ 1 2) => 3', function() {
            let inst = '(+ 1 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 3");
        });

        it('(+ .5 1) => 1.500000', function() {
            let inst = '(+ .5 1)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 1.500000");
        });
    });

    describe('Subtraction', function() {

        it('(- 5 2) => 3', function() {
            let inst = '(- 5 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 3");
        });

        it('(- 5 .1) => 4.900000', function() {
            let inst = '(- 5 .1)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 4.900000");
        });
    });

    describe('Multiplication', function() {

        it('(* 3 2) => 6', function() {
            let inst = '(* 3 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 6");
        });

        it('(* 5 3.0) => 15.000000', function() {
            let inst = '(* 5 3.0)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 15.000000");
        });
    });

    describe('Division', function() {

        it('(/ 15 3) => 5', function() {
            let inst = '(/ 15 3)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 5");
        });

        it('(/ 10 3.0) => 3.333333', function() {
            let inst = '(/ 10 3.0)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 3.333333");
        });
    });

    describe('Increase', function() {

        it('(inc 1) => 2', function() {
            let inst = '(inc 1)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 2");
        });

        it('(inc 1.5) => 2.500000', function() {
            let inst = '(inc 1.5)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 2.500000");
        });
    });

    describe('Decrease', function() {

        it('(dec 2) => 1', function() {
            let inst = '(dec 2)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> 1");
        });

        it('(dec .5) => -0.500000', function() {
            let inst = '(dec .5)';
            let stdout = run(inst).stdout;
            expect(stdout).to.contain.string("> -0.500000");
        });
    });
});
