# Typescheme

The *Typescheme* programming language.

## Index

* [About](#markdown-header-about)
* [Setup](#markdown-header-setup)
    * [Build](#markdown-header-build)
    * [Test](#markdown-header-test)
* [Language Features](#markdown-header-language-features)
* [Use](#markdown-header-use)
    * [REPL](#markdown-header-repl)
    * [With source files](#markdown-header-with-source-files)

## About

Formal Languages and Compilers course project.

## Setup

Clone the repository:

```text
git clone https://bitbucket.org/teama8/typescheme.git
```

or download the source code [here](https://bitbucket.org/teama8/typescheme/downloads/).

### Build

To create an executable `bin/typescheme` run 

```text
make
```

in the project directory, or, alternatively run the single commands in your terminal

```text
mkdir -p ./lib/ ./bin/
bison -vdy -o./lib/typescheme.tab.c ./src/typescheme.y
flex -l -o./lib/typescheme.yy.c ./src/typescheme.l
gcc -I./src/ ./src/utils.c \
    ./src/operations.c ./src/SymbolTable.c \
    ./src/TypedValue.c ./src/StringReplace.c \
    ./lib/typescheme.tab.c ./lib/typescheme.yy.c \
    -o./bin/typescheme
```

### Test

To run the tests, you need [`Node.js`](https://nodejs.org) and [`npm`](https://www.npmjs.com/) installed on your system.

To install the needed dependencies, run

```
npm install
```

To run the test files, run

```
npm run test
```


## Language features

See the file [`documentation.cl`](documentation.cl) or the example programs in the [`examples`](examples) directory.

## Use

### REPL

From the project root directory, run the executable `bin/typescheme` to start a REPL, this will show the following prompt, where single instructions can be run.

```text
TypeScheme 0.1.0
Type ^C to quit
>
```

### With source files

Supply a file with instructions to the call of the executable, e.g.

```text
bin/typescheme examples/hello.cl
```