; This program creates a local scope
(var c 4)
(local
    (var c "Hello")
    (display c))
(display c)