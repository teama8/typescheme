;Performs various arithmetic operations and shows result
(display (concat "5 + 6 = " (+ 5 6)))
(display (concat "7 - 3 = " (- 7 3)))
(display (concat "2 * 4 = " (* 2 4)))
(display (concat "9 / 3 = " (/ 9 3)))