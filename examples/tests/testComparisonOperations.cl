;Performs various comparison operations and shows result
(display (concat "5 < 6     ? " (< 5 6)))
(display (concat "7 >= 8    ? " (>= 7 8)))
(display (concat "5 = 2 + 3 ? " (= 5 (+ 2 3))))