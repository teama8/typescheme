;Performs various logic operations and shows result
(display (concat "#t and #f = " (and #t #f)))
(display (concat "#f or #t  = " (or #f #t)))
(display (concat "not #f    = " (not #f)))