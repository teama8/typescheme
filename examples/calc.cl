; Simple calculator
(read name "Enter your name: ")
(display (concat (concat "Hello, " name) "!\n"))

(read fst "This is a simple calculator.\nEnter a number: ")
(read snd "Enter another number: ")

(display (concat 
    "\nWhich operation would you like to perform?\n"
    "Enter: [1] addition, [2] subtraction, [3] multiplication or [4] division: "))

(read op "\nYour choice [1-4]: ")

(display (if (or (<= op 0) (> op 4))
    (concat (concat "\n" op) " is not an option!")
    (concat "\nThe result is: "
        (if (= 1 op)
            (+ fst snd)
            (if (= 2 op)
                (- fst snd)
                (if (= 3 op)
                    (* fst snd)
                    (/ fst snd)))))))