; Asks user for a number and squares it
(read n "Enter a number: ")
(var square (* n n))
(display (concat 
    (concat "The square of " n)
    (concat " is " square)))