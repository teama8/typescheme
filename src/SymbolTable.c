#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SymbolTable.h"

void printNode(struct Node *node)
{
    pprint("(%s, %d, %TYPE, %VAL)\n", node->tv, 
            node->name, node->scope); 
}

void printSymbolTable(struct SymbolTable *st)
{
    printf("Symbol Table with current scope '%d'. Content:\n", st->currScope);
    struct Node *ptr = st->head;
    while(ptr != NULL) {
        printNode(ptr);
        ptr = ptr->next;
    }
}

void addStEntry(char* name, struct TypedValue *tv, struct SymbolTable *st) {

    /* Creates the new node */
    struct Node *node = (struct Node*) malloc(sizeof(struct Node));
    node->name = name;
    node->scope = st->currScope;
    node->tv = tv;
    node->next = NULL;

    /* The new node is added at the beginning of the list */
    node->next = st->head;
    st->head = node;
}

struct SymbolTable* newSymbolTable()
{
    struct SymbolTable *st = (struct SymbolTable*) malloc(sizeof(struct SymbolTable));
    st->currScope = 0;
    st->head = NULL;
    return st;
}

int openScope(struct SymbolTable *st)
{
    st->currScope++;
    return st->currScope;
}

int closeScope(struct SymbolTable *st)
{
    if (st->currScope == 0) {
        printf("SystemError: cannot close global scope");
        return -1;
    }

    // Check if the first node has to be deleted.
    // If yes, it is deleted and the new first node is checked.
    while (st->head != NULL && 
            st->head->scope == st->currScope){
        struct Node *deleted = st->head;
        st->head = st->head->next;

        // Freeing the deleted node
        free(deleted->name);
        free(deleted->tv);
        free(deleted);
    }

    if (st->head == NULL) {
        return st->currScope--;
    }

    // The rest of the list is checked
    struct Node *curr = st->head;
    struct Node *deleted = NULL;

    while (curr->next != NULL) {
        if (curr->next->scope == st->currScope) {
            deleted = curr->next;
            curr->next = curr->next->next;

            deleted->next = NULL;
            free(deleted->name);
            free(deleted->tv);
            free(deleted);
        } 

        curr = curr->next;
    }

    return st->currScope--;
}

struct Node* findNodeByName(char* name, struct SymbolTable *st)
{
    struct Node *res = NULL;
    struct Node *ptr = st->head;

    while(ptr != NULL) {
        if(strcmp(ptr->name, name) == 0) {
            if (res == NULL || (ptr->scope > res->scope)){
                res = ptr;
            }
        }

        ptr = ptr->next;
    }
    return res;
}
