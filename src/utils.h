#ifndef UTILS_H
#define UTILS_H

// Set to 1 if interactive mode, 0 otherwise
int INTERACTIVE;

// Error handling
void throwError(char *errType, char *errMsg);

#endif