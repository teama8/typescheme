#ifndef TYPED_VALUE_H
#define TYPED_VALUE_H

// Contains all the available data types
enum datatype {
    INTEGER, FLOAT, BOOLEAN, VOID, STRING
};

// Represents a generic value with associated information
// about its type.
struct TypedValue {
    void* value;
    enum datatype type;
};

// Gives a string representation of a datatype
char* typeToString(enum datatype type);

// Returns a string representation of the value
char* valueToString(struct TypedValue *tv);

// Polymorfic printf which allows to print any
// TypedValue without littering the code with switches. The
// str string can contain two patterns: %VAL and %TYPE. The first
// one will be substituted with a string representation of the value
// contained in the struct, the second one will be replaced with a string
// representation of the type. The vararg argument will be passed directly to
// printf.
void pprint(const char* str, const struct TypedValue *tv, ...);

// Instantiates a new TypedValue and allocates
// the memory for the value
struct TypedValue* newTypedValue(enum datatype type);

#endif
