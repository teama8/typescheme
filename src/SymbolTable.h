#ifndef SYMBOL_TABLE_H
#define SYMBOL_TABLE_H

#include "TypedValue.h"

struct Node {
    char* name;
    int scope;
    struct TypedValue *tv;
    struct Node *next;
};

struct SymbolTable {
    int currScope;
    struct Node *head;
};

// Prints the content of a single node
void printNode(struct Node *node);

// Prints the content of the entire symbol table
void printSymbolTable(struct SymbolTable *st);

// Adds a new node with name and value to the beginning of the
// symbol table. This new node will be created in the current
// scope of the symbol table
void addStEntry(char* name, struct TypedValue *tv, struct SymbolTable *st);

// Creates a new symbol table
struct SymbolTable* newSymbolTable();

// Opens a new scope in the Symbol Table. The number of the
// new scope is returned by the function.
int openScope(struct SymbolTable *st);

// Closes the current scope in the Symbol Table. The number of the
// closed scope is returned by the function. All the nodes created
// within the closed scope are removed from the Symbol Table.
int closeScope(struct SymbolTable *st);

// Finds a node inside a symbol table given the name
// If the name does not exist, returns NULL. This methods gives precedence
// to the node with name "name" which has scope "i" such that "st->currScope - i"
// is minimum. This means that this method returns the node declared in the closest
// enclosing scope (possibily the current one).
struct Node* findNodeByName(char* name, struct SymbolTable *st);

#endif
