#define _GNU_SOURCE
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "TypedValue.h"
#include "utils.h"
#include "Constants.h"

int numOpTypeCheck(char *opType, enum datatype ops[], int size);
int boolOpTypeCheck(char *opType, enum datatype ops[], int size);

// TODO: refactor code, avoid duplicate instructions

struct TypedValue* stringConcat(struct TypedValue *fst, struct TypedValue *snd)
{
    struct TypedValue *res = newTypedValue(STRING);
    char *fstStr;
    char *sndStr;
    char *str;

    // convert first arg to string
    fstStr = valueToString(fst);

    // convert second arg to string
    sndStr = valueToString(snd);

    asprintf(&str, "%s%s", fstStr, sndStr);
    res->value = str;
    return res;
}

struct TypedValue* arithmeticOP(char *op, struct TypedValue *ops[], int size)
{
    struct TypedValue *res;
    int typeErr;
    if(size == 2) { 
        enum datatype a[2] = { ops[0]->type, ops[1]->type };
        typeErr = numOpTypeCheck("arithmetic", a, size);
    } else {
        enum datatype a[1] = { ops[0]->type };
        typeErr = numOpTypeCheck("arithmetic", a, size);
    }

    // Check for type errors
    if(typeErr == 1) {
        res = newTypedValue(VOID);
    }
    // No errors
    else {
        // two args, either +, -, *, or /
        if(size == 2) {
            // Both operands are ints
            if(ops[0]->type == INTEGER && ops[1]->type == INTEGER) {
                res = newTypedValue(INTEGER);
                int a = *((int *)(ops[0]->value));
                int b = *((int *)(ops[1]->value));

                if(strcmp(op, "+") == 0) {
                    *((int *)(res->value)) = a + b;
                } else if(strcmp(op, "-") == 0) {
                    *((int *)(res->value)) = a - b;
                } else if(strcmp(op, "*") == 0) {
                    *((int *)(res->value)) = a * b;
                } else if(strcmp(op, "/") == 0) {
                    // check if divisor is zero
                    if(b == 0) {
                        res = newTypedValue(VOID);
                        throwError("DivideByZeroError", 
                            "not going to explain this one");
                    } else { *((int *)(res->value)) = a / b; }
                }
                // None of the above, thus wrong arity
                else {
                    res = newTypedValue(VOID);
                    char *errMsg;
                    asprintf(&errMsg, "wrong number of args passed to operator %s", op);
                    throwError("ArityError", errMsg);
                }
            }
            // Float operation
            else {
                res = newTypedValue(FLOAT);
                float a;
                float b;
                if(ops[0]->type == INTEGER) {
                    a = (float)*((int *)(ops[0]->value));
                } else {
                    a = *((float *)(ops[0]->value));
                }
                if(ops[1]->type == INTEGER) {
                    b = (float)*((int *)(ops[1]->value));
                } else {
                    b = *((float *)(ops[1]->value));
                }

                if(strcmp(op, "+") == 0) {
                    *((float *)(res->value)) = a + b;
                } else if(strcmp(op, "-") == 0) {
                    *((float *)(res->value)) = a - b;
                } else if(strcmp(op, "*") == 0) {
                    *((float *)(res->value)) = a * b;
                } else if(strcmp(op, "/") == 0) {
                    // check if divisor is zero
                    if(b == .0) {
                        res = newTypedValue(VOID);
                        throwError("DivideByZeroError", 
                            "not going to explain this one");
                    } else { *((float *)(res->value)) = a / b; }
                } 
                // None of the above, thus wrong arity
                else {
                    res = newTypedValue(VOID);
                    char *errMsg;
                    asprintf(&errMsg, "wrong number of args passed to operator %s", op);
                    throwError("ArityError", errMsg);
                }
            }
        }
        // either inc or dec
        else {
            // Float operation
            if(ops[0]->type == FLOAT) {
                res = newTypedValue(FLOAT);
                float a = *((float *)(ops[0]->value));

                if(strcmp(op, "inc") == 0) {
                    *((float *)(res->value)) = a + 1;
                } else if(strcmp(op, "dec") == 0) {
                    *((float *)(res->value)) = a - 1;
                }
                // None of the above, thus wrong arity
                else {
                    res = newTypedValue(VOID);
                    char *errMsg;
                    asprintf(&errMsg, "wrong number of args passed to operator %s", op);
                    throwError("ArityError", errMsg);
                }
            }
            // Int operation
            else {
                res = newTypedValue(INTEGER);
                int a = *((int *)(ops[0]->value));

                if(strcmp(op, "inc") == 0) {
                    *((int *)(res->value)) = a + 1;
                } else if(strcmp(op, "dec") == 0) {
                    *((int *)(res->value)) = a - 1;
                }
                // None of the above, thus wrong arity
                else {
                    res = newTypedValue(VOID);
                    char *errMsg;
                    asprintf(&errMsg, "wrong number of args passed to operator %s", op);
                    throwError("ArityError", errMsg);
                }
            }
        }
    }

    return res;
}

struct TypedValue* comparisonOP(char *op, struct TypedValue *ops[], int size)
{
    struct TypedValue *res;
    int typeErr;
    if(size == 2) { 
        enum datatype a[2] = { ops[0]->type, ops[1]->type };
        typeErr = numOpTypeCheck("comparison", a, size);
    }

    // Check for type errors
    if(typeErr == 1) {
        res = newTypedValue(VOID);
    }
    // No errors
    else {
        // two args
        if(size == 2) {
            // Both operands are ints
            if(ops[0]->type == INTEGER && ops[1]->type == INTEGER) {
                res = newTypedValue(BOOLEAN);
                enum boolean r; 
                int a = *((int *)(ops[0]->value));
                int b = *((int *)(ops[1]->value));

                if(strcmp(op, ">") == 0) {
                    r = (a > b) ? TRUE : FALSE;
                } else if(strcmp(op, "<") == 0) {
                    r = (a < b) ? TRUE : FALSE;
                } else if(strcmp(op, ">=") == 0) {
                    r = (a >= b) ? TRUE : FALSE;
                } else if(strcmp(op, "<=") == 0) {
                    r = (a <= b) ? TRUE : FALSE;
                } else if(strcmp(op, "=") == 0) {
                    r = (a == b) ? TRUE : FALSE;
                } else {
                    r = (a != b) ? TRUE : FALSE;
                }
                *((enum boolean *)(res->value)) = r;
            }
            // Float operation
            else {
                res = newTypedValue(BOOLEAN);
                enum boolean r; 
                float a;
                float b;
                if(ops[0]->type == INTEGER) {
                    a = (float)*((int *)(ops[0]->value));
                } else {
                    a = *((float *)(ops[0]->value));
                }
                if(ops[1]->type == INTEGER) {
                    b = (float)*((int *)(ops[1]->value));
                } else {
                    b = *((float *)(ops[1]->value));
                }

                if(strcmp(op, ">") == 0) {
                    r = (a > b) ? TRUE : FALSE;
                } else if(strcmp(op, "<") == 0) {
                    r = (a < b) ? TRUE : FALSE;
                } else if(strcmp(op, ">=") == 0) {
                    r = (a >= b) ? TRUE : FALSE;
                } else if(strcmp(op, "<=") == 0) {
                    r = (a <= b) ? TRUE : FALSE;
                } else if(strcmp(op, "=") == 0) {
                    r = (a == b) ? TRUE : FALSE;
                } else {
                    r = (a != b) ? TRUE : FALSE;
                }
                *((enum boolean *)(res->value)) = r;
            }
        }
    }

    return res;
}

struct TypedValue* logicOP(char *op, struct TypedValue *ops[], int size)
{
    struct TypedValue *res;
    int typeErr;
    if(size == 2) { 
        enum datatype a[2] = { ops[0]->type, ops[1]->type };
        typeErr = boolOpTypeCheck("logic", a, size);
    } else {
        enum datatype a[1] = { ops[0]->type };
        typeErr = boolOpTypeCheck("logic", a, size);
    }

    // Check for type errors
    if(typeErr == 1) {
        res = newTypedValue(VOID);
    }
    // No errors
    else {
        // two args, either and or or
        if(size == 2) {
            res = newTypedValue(BOOLEAN);
            enum boolean r;
            enum boolean a = *((enum boolean *)(ops[0]->value));
            enum boolean b = *((enum boolean *)(ops[1]->value));

            if(strcmp(op, "and") == 0) {
                r = (a == TRUE && b == TRUE) ? TRUE : FALSE;
                *((enum boolean *)(res->value)) = r;
            } else if(strcmp(op, "or") == 0) {
                r = (a == TRUE || b == TRUE) ? TRUE : FALSE;
                *((enum boolean *)(res->value)) = r;
            }
            // None of the above, thus wrong arity
            else {
                res = newTypedValue(VOID);
                char *errMsg;
                asprintf(&errMsg, "wrong number of args passed to operator %s", op);
                throwError("ArityError", errMsg);
            }
        }
        // not
        else {
            res = newTypedValue(BOOLEAN);
            enum boolean r;
            enum boolean a = *((enum boolean *)(ops[0]->value));

            if(strcmp(op, "not") == 0) {
                r = (a == TRUE) ? FALSE : TRUE;
                *((enum boolean *)(res->value)) = r;
            }
            // None of the above, thus wrong arity
            else {
                res = newTypedValue(VOID);
                char *errMsg;
                asprintf(&errMsg, "wrong number of args passed to operator %s", op);
                throwError("ArityError", errMsg);
            }
        }
    }

    return res;
}

int numOpTypeCheck(char *opType, enum datatype ops[], int size)
{
    int typeErr = 0;
    char *errMsg;
    if(size == 2) {
        if (ops[0] != INTEGER && ops[0] != FLOAT) {
            typeErr = 1;
            asprintf(&errMsg, "first operand of %s operation is not numerical. It is %s",
                opType, typeToString(ops[0]));
        } else if (ops[1] != INTEGER && ops[1] != FLOAT) {
            typeErr = 1;
            asprintf(&errMsg, "second operand of %s operation is not numerical. It is %s",
                opType, typeToString(ops[1]));
        }
    } else {
        if (ops[0] != INTEGER && ops[0] != FLOAT) {
            typeErr = 1;
            asprintf(&errMsg, "operand of %s operation is not numerical. It is %s",
                opType, typeToString(ops[0]));
        }
    }

    if(typeErr == 1) {
        throwError("TypeError", errMsg);
    }
    return typeErr;
}

int boolOpTypeCheck(char *opType, enum datatype ops[], int size)
{
    int typeErr = 0;
    char *errMsg;
    if(size == 2) {
        if (ops[0] != BOOLEAN) {
            typeErr = 1;
            asprintf(&errMsg, "first operand of %s operation has type %s, expected BOOLEAN",
                opType, typeToString(ops[0]));
        } else if (ops[1] != BOOLEAN) {
            typeErr = 1;
            asprintf(&errMsg, "second operand of %s operation has type %s, expected BOOLEAN",
                opType, typeToString(ops[1]));
        }
    } else {
        if (ops[0] != BOOLEAN) {
            typeErr = 1;
            asprintf(&errMsg, "operand of %s operation has type %s, expected BOOLEAN",
                opType, typeToString(ops[0]));
        }
    }

    if(typeErr == 1) {
        throwError("TypeError", errMsg);
    }
    return typeErr;
}

int isNumeric(const char *s)
{
    if (s == NULL || *s == '\0' || isspace(*s)) { return 0; }
    char * p;
    strtod (s, &p);
    return *p == '\0';
}