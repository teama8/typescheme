%{
#define _GNU_SOURCE
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>

#include "SymbolTable.h"
#include "TypedValue.h"
#include "Constants.h"
#include "operations.h"
#include "utils.h"

extern FILE* yyin;

struct SymbolTable *st;

static const char PROMPT[] = "> ";
static const char MOTD[] = "TypeScheme 0.1.0\nType ^C to quit";

void yyerror(char *s);
int yylex();
%}

%union {
    char* lexeme;
    char* operator;
    int scope;
    struct TypedValue *tv;
}

%token <tv> LITERAL
%token <lexeme> ID

%token <operator> AOP
%token <operator> COP
%token <operator> LOP
%token VAR
%token SET
%token DISPLAY
%token READ
%token IF
%token LOCAL
%token CONCAT

%type <tv> expr
%type <tv> sub_program
%type <scope> open_scope_marker
%type <scope> close_scope_marker

%start program

%%

program:
    /* empty */ {}
    | program expr
        { 
            //pprint("=> DEBUG: Expression has type %TYPE\n=> %VAL\n", $2);
            if(INTERACTIVE == 1) {
                // TODO: eventually fix this
                if($2->type == STRING) {
                    pprint("\"%VAL\"\n%s", $2, PROMPT);
                } else {
                    pprint("%VAL\n%s", $2, PROMPT);
                }
            }
        }
    ;

sub_program:
           /* epsilon */ {}
           | sub_program expr {}
           ;

open_scope_marker:
                /* epsilon */ { $$ = openScope(st); }
                ;

close_scope_marker:
                /* epsilon */ { $$ = closeScope(st); }
                ;
                
expr:
    LITERAL { $$ = $1; }
    | ID    
        {
            struct Node* n = findNodeByName($1, st); 
            if(n == NULL) {
                char *errMsg;
                asprintf(&errMsg, "Variable %s not declared", $1);
                throwError("ReferenceError", errMsg);
                $$ = newTypedValue(VOID);
            } else {
                $$ = n->tv;
            }
        }
    | '(' IF expr expr expr ')'
        {
            enum datatype t[1] = { $3->type };
            int typeErr = boolOpTypeCheck("conditional", t, 1);
            
            // Check for type errors
            if(typeErr == 1) {
                $$ = newTypedValue(VOID);
            }
            // No errors
            else {
                enum boolean *condition = (enum boolean *)($3->value);
                if((*condition) == TRUE) { $$ = $4; }
                else { $$ = $5; }
            }
        }
    | '(' DISPLAY expr ')'
        {
            pprint("%VAL\n", $3);
            $$ = newTypedValue(VOID);
        }
    | '(' READ ID expr ')'
        {
            char str[100]; //TODO: Missing bound checks
            pprint("%VAL", $4);
            scanf("%s", str);

            struct TypedValue *tv;
            // determine type
            if(isNumeric(str) == 1) {
                tv = newTypedValue(FLOAT);
                *((float *)(tv->value)) = atof(str);
            } else {
                tv = newTypedValue(STRING);
                tv->value = str;
            }

            struct Node *node = findNodeByName($3, st);
            if(node == NULL || node->scope < st->currScope) {
                addStEntry($3, tv, st);
            } else {
                char *errMsg;
                asprintf(&errMsg, "Variable %s already defined in the current scope", $3);
                throwError("ReferenceError", errMsg);
            }
            $$ = newTypedValue(VOID);
        }
    | '(' CONCAT expr expr ')'
        {
            $$ = stringConcat($3, $4);
        }
    | '(' VAR ID expr ')'
        {
            struct Node *node = findNodeByName($3, st);
            if(node == NULL || node->scope < st->currScope) {
                addStEntry($3, $4, st);
            } else {
                char *errMsg;
                asprintf(&errMsg, "Variable %s already defined in the current scope", $3);
                throwError("ReferenceError", errMsg);
            }
            $$ = newTypedValue(VOID);
        }
    | '(' AOP expr ')'
        {
            struct TypedValue* tvs[1] = { $3 };
            $$ = arithmeticOP($2, tvs, 1);
        }
    | '(' AOP expr expr ')'
        {
            struct TypedValue* tvs[2] = { $3, $4 };
            $$ = arithmeticOP($2, tvs, 2);
        }
    | '(' COP expr expr ')'
        {
            struct TypedValue* tvs[2] = { $3, $4 };
            $$ = comparisonOP($2, tvs, 2);
        }
    | '(' LOP expr ')'
        {
            struct TypedValue* tvs[1] = { $3 };
            $$ = logicOP($2, tvs, 1);
        }
    | '(' LOP expr expr ')'
        {
            struct TypedValue* tvs[2] = { $3, $4 };
            $$ = logicOP($2, tvs, 2);
        }
    | '(' LOCAL open_scope_marker sub_program close_scope_marker ')'
        {
            $$ = newTypedValue(VOID);
        }
    | '(' SET ID expr ')'
        {
            struct Node *node = findNodeByName($3, st);
            if(node != NULL) {
                node->tv = $4;    
            } else {
                char *errMsg;
                asprintf(&errMsg, "Variable %s is not defined", $3);
                throwError("ReferenceError", errMsg);
            }
            $$ = newTypedValue(VOID);
        }
    |  '(' expr ')' { $$ = $2; }
    ;

%%

/*
 * Yacc error handling
 */
void yyerror(char *s)
{
    throwError("SyntaxError", s);
}

/*
 * Main method
 */
int main(int argc, char** argv)
{
    st = newSymbolTable();
    if(argc==2) {
        yyin = fopen(argv[1], "r");
        if(!yyin) {
            char *errMsg;
            asprintf(&errMsg, "Can't read file %s", argv[1]);
            throwError("Error", errMsg);
        }
        do {
            yyparse();
        } while(!feof(yyin));
    } else {
        INTERACTIVE = 1;
        printf("%s\n%s", MOTD, PROMPT);
        yyparse();
    }
    exit(0);
}
