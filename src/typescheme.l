%option noyywrap
%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "StringReplace.h"
#include "typescheme.tab.h"

#include "TypedValue.h"
#include "Constants.h"
%}

DIGIT       [0-9]
LETTER      [a-zA-Z_]
STRING      \"[^"]*\"
ID          {LETTER}+({DIGIT}|{LETTER})*
NUM         {DIGIT}+
FLOAT       {DIGIT}*\.{DIGIT}+
LTERM       \r|\n|\r\n
WSPACE      {LTERM}|[ \t\f] 
COMMENT     ;[^\r\n]*\n?
BOOL        (#t|#f)
AOP         (\+|-|\*|\/|inc|dec)
COP         (<|>|<=|>=|=|!=)
LOP         (and|or|not)

%%

"var"       { return VAR; }
"set"       { return SET; }
"display"   { return DISPLAY; }
"read"      { return READ; }
"concat"    { return CONCAT; }
"if"        { return IF; }
"local"     { return LOCAL; }
"("         { return '('; }
")"         { return ')'; }


{LOP}   {
    yylval.operator = strdup(yytext);
    return LOP;
}

{AOP}   {
    yylval.operator = strdup(yytext);
    return AOP;
}

{COP}   {
    yylval.operator = strdup(yytext);
    return COP;
}

{BOOL}  {
    yylval.tv = newTypedValue(BOOLEAN);

    enum boolean *val = yylval.tv->value;
    if(strcmp(yytext, "#t") == 0) {
       *val = TRUE;
    } else {
       *val = FALSE;
    }

    return LITERAL;
 }

{STRING} {
    yylval.tv = newTypedValue(STRING);

    //Removing " " from begininning and end of the string
    int l = strlen(yytext);
    char *trimmed = malloc((sizeof (char)) * (l - 2) + 1);
    if (trimmed == NULL) exit(1);
    for(int i = 1; i <= l-2; i++){
        trimmed[i - 1] = yytext[i];
    }
    trimmed[l - 2] = (char)0;

    char *fixed = repl_str(trimmed, "\\n", "\n");
    yylval.tv->value = fixed;

    return LITERAL;
}

{ID}    {
    yylval.lexeme = strdup(yytext);
    return ID;
}

{NUM}   { 
    yylval.tv = newTypedValue(INTEGER);
    *((int *)(yylval.tv->value)) = atoi(yytext);

    return LITERAL;
}

{FLOAT} { 
    yylval.tv = newTypedValue(FLOAT);
    *((float *)(yylval.tv->value)) = atof(yytext);

    return LITERAL;
}

{LTERM} {}

{WSPACE}
        {}

{COMMENT}
        {}

%%
