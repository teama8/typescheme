#ifndef OPERATIONS_H
#define OPERATIONS_H

// Concatenates two parameters to a single string
struct TypedValue* stringConcat(struct TypedValue *fst, struct TypedValue *snd);

// Performs an arithmetic operation on one or two numeric operands
struct TypedValue* arithmeticOP(char *op, struct TypedValue *ops[], int size);

// Performs a comparison operation on two numeric operands
struct TypedValue* comparisonOP(char *op, struct TypedValue *ops[], int size);

// Perform a logic operation on one or two boolean operands
struct TypedValue* logicOP(char *op, struct TypedValue *ops[], int size);

// Perform type check on operands for numeric operations 
int numOpTypeCheck(char *opType, enum datatype ops[], int size);

// Perform type check on operands for boolean operations 
int boolOpTypeCheck(char *opType, enum datatype ops[], int size);

// Returns true (non-zero) if character-string parameter represents a signed
// or unsigned floating-point number. Otherwise returns false (zero). 
int isNumeric(const char * s);

#endif