#include <stdio.h>
#include <stdlib.h>

int INTERACTIVE = 0;

void throwError(char *errType, char *errMsg)
{
    fprintf(stdout, "%s: %s\n", errType, errMsg);
    if(INTERACTIVE == 0) { exit(1); }
}