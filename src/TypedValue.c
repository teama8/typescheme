#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "TypedValue.h"
#include "Constants.h"
#include "StringReplace.h"

char* typeToString(enum datatype type){
    switch(type){
        case INTEGER:
            return "INTEGER";
            break;
        case FLOAT:
            return "FLOAT";
            break;
        case BOOLEAN:
            return "BOOLEAN";
            break;
        case VOID:
            return "VOID";
            break;
        case STRING:
            return "STRING";
        default:
            return NULL;
    }
}

char* valueToString(struct TypedValue *tv)
{
    char *str;
    char *tmp;
    switch(tv->type) {
        case INTEGER:
            asprintf(&str, "%d", *((int *)(tv->value)));
            break;
        case FLOAT:
            asprintf(&str, "%f", *((float *)(tv->value)));
            break;
        case STRING:
            asprintf(&str, "%s", tv->value);
            break;
        case BOOLEAN:
            tmp = (*((enum boolean *)(tv->value)) == TRUE ? "#t" : "#f");
            str = strdup(tmp);
            break;
        default:
            str = strdup("");
    }
    return str;
}

void pprint(const char *str, const struct TypedValue *tv, ...){
    char *val_str = NULL;
    char *type_str = typeToString(tv->type);

    char *tmp = NULL;
    int l;

    switch(tv->type){
        case INTEGER:
            val_str = malloc(sizeof(int) * 8 + 1); //size of MAX_INT as string
            if (val_str == NULL) exit(1); //TODO
            sprintf(val_str,"%d", *((int *)(tv->value)));
            break;
        case FLOAT:
            val_str = malloc(sizeof(float) * 8 + 1);
            if (val_str == NULL) exit(1); //TODO
            sprintf(val_str,"%f", *((float *)(tv->value)));
            break;
        case BOOLEAN:
            val_str = malloc(sizeof(char) * 2 + 1); // size of #t and #f as string
            if (val_str == NULL) exit(1); //TODO
            tmp = (*((enum boolean *)(tv->value)) == TRUE ? "#t" : "#f");
            val_str = strdup(tmp);
            break;
        case VOID:
            val_str = malloc(sizeof(char) * 4 + 1); // size of "void"
            if (val_str == NULL) exit(1);
            val_str = strdup("void");
            break;
        case STRING:
            /* NOTE: as of now return values are manually
            padded with '"' in typescheme.y.
            Logics to add '"' for every print are commented below,
            if things change in future, edit. */

            // ~~Adding " and " at beginning and end~~
            tmp = strdup((char *)(tv->value));
            l = strlen(tmp);
            val_str = malloc((sizeof (char)) * (l/* + 2*/) + 1);
            if (val_str == NULL) exit(1);
            //val_str[0] = '"';
            //val_str[l + 1] = '"';
            for (int i = 0; i < l; i++){
                val_str[i/* + 1*/] = tmp[i];
            } 
            val_str[l/* + 2*/] = '\0';
            break;
        default:
            printf("Fail silently?");
            return;
    }

    //TODO memory leak
    char *res = repl_str(str, "%VAL", val_str);
    res = repl_str(res, "%TYPE", type_str);

    va_list argptr;
    va_start(argptr, tv);
    vfprintf(stdout, res, argptr);
    va_end(argptr);

    free(val_str);
    free(res);
}

struct TypedValue* newTypedValue(enum datatype type){

    // Initializes the struct
    struct TypedValue* tv = malloc(sizeof (struct TypedValue));
    if (tv == NULL) exit(1); //TODO

    // Initializes the memory for the value to be contained
    // in the struct
    void* v;
    switch (type){
        case INTEGER:
            v = malloc(sizeof (int));
            break;
        case FLOAT:
            v = malloc(sizeof (float));
            break;
        case BOOLEAN:
            v = malloc(sizeof (enum boolean));
            break;
        case VOID:
        case STRING:
            v = NULL;
            break;
        default:
            printf("Fail silently?");
    }

    if (v == NULL && type != VOID && type != STRING) exit(1); //TODO

    tv->value = v;
    tv->type = type;
    return tv;
}
