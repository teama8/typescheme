/**
 * Test program for typescheme
 */

let shell = require('shelljs'),
    path = require('path'),
    fs = require('fs');

let testdir = path.join(__dirname, "examples/tests");
let bin = path.join(__dirname, 'bin/typescheme');
let files = [];

// Run a testfile
let run = (file) => {
    console.log("\nRunning \'%s\':", file);
    let testfile = path.join(testdir, file);
    let src = fs
        .readFileSync(testfile)
        .toString('utf-8')
        .trim();
    console.log("\x1b[2m%s\x1b[0m", src);
    let res = shell.exec(`"${bin}" "${testfile}"`, {
        silent: true
    }).stdout.trim();
    console.log(res);
};

// Save files in testdir in an array
fs.readdirSync(testdir).forEach(file => {
    files.push(file);
});

console.log("Test script for \'typescheme\'\n" +
            "Running test files. (%d found)", files.length);

// Run all testfiles
files.forEach(file => run(file));
